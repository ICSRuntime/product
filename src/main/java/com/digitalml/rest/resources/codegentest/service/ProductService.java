package com.digitalml.rest.resources.codegentest.service;
    	
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.net.URL;

import org.apache.commons.collections.CollectionUtils;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.SecurityContext;
import java.security.AccessControlException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.*;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

// Import any model objects used by the interface

import com.digitalml.rest.resources.codegentest.*;

/**
 * Service: Product
 * Access and update of insurance product information.0
 *
 * This service has been automatically generated by Ignite
 *
 * @author admin
 * @version 1.0
 *
 */

public abstract class ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

	// Required for JSR-303 validation
	static private ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

	protected static Mapper mapper;

	static {
		URL configFile = ProductService.class.getResource("ProductServiceMappings.xml");
		if (configFile != null) {

			List<String> mappingFiles = new ArrayList<String>();
			mappingFiles.add(configFile.toExternalForm());
			mapper = new DozerBeanMapper(mappingFiles);

		} else {
			mapper = new DozerBeanMapper(); // Use default wildcard mappings only
		}
	}

	protected boolean checkPermissions(SecurityContext securityContext) throws AccessControlException {
		return true;
	}

	/**
	Implements method getaspecificproduct
	
		Gets product details
	*/
	public GetASpecificProductReturnDTO getaspecificproduct(SecurityContext securityContext, GetASpecificProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method getaspecificproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access getaspecificproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access getaspecificproduct");

		GetASpecificProductReturnDTO returnValue = new GetASpecificProductReturnDTO();
        GetASpecificProductCurrentStateDTO currentState = new GetASpecificProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method findproduct
	
		Gets a collection of product details filtered 
	*/
	public FindProductReturnDTO findproduct(SecurityContext securityContext, FindProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method findproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access findproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access findproduct");

		FindProductReturnDTO returnValue = new FindProductReturnDTO();
        FindProductCurrentStateDTO currentState = new FindProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method updateproduct
	
		Updates product
	*/
	public UpdateProductReturnDTO updateproduct(SecurityContext securityContext, UpdateProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method updateproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access updateproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access updateproduct");

		UpdateProductReturnDTO returnValue = new UpdateProductReturnDTO();
        UpdateProductCurrentStateDTO currentState = new UpdateProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method replaceproduct
	
		Replaces product
	*/
	public ReplaceProductReturnDTO replaceproduct(SecurityContext securityContext, ReplaceProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method replaceproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access replaceproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access replaceproduct");

		ReplaceProductReturnDTO returnValue = new ReplaceProductReturnDTO();
        ReplaceProductCurrentStateDTO currentState = new ReplaceProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method createproduct
	
		Creates product
	*/
	public CreateProductReturnDTO createproduct(SecurityContext securityContext, CreateProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method createproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access createproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access createproduct");

		CreateProductReturnDTO returnValue = new CreateProductReturnDTO();
        CreateProductCurrentStateDTO currentState = new CreateProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method deleteproduct
	
		Deletes product
	*/
	public DeleteProductReturnDTO deleteproduct(SecurityContext securityContext, DeleteProductInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method deleteproduct");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access deleteproduct");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access deleteproduct");

		DeleteProductReturnDTO returnValue = new DeleteProductReturnDTO();
        DeleteProductCurrentStateDTO currentState = new DeleteProductCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}


    // Supporting Use Case and Process methods

	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep1(GetASpecificProductCurrentStateDTO currentState);
	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep2(GetASpecificProductCurrentStateDTO currentState);
	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep3(GetASpecificProductCurrentStateDTO currentState);
	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep4(GetASpecificProductCurrentStateDTO currentState);
	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep5(GetASpecificProductCurrentStateDTO currentState);
	public abstract GetASpecificProductCurrentStateDTO getaspecificproductUseCaseStep6(GetASpecificProductCurrentStateDTO currentState);


	public abstract FindProductCurrentStateDTO findproductUseCaseStep1(FindProductCurrentStateDTO currentState);
	public abstract FindProductCurrentStateDTO findproductUseCaseStep2(FindProductCurrentStateDTO currentState);
	public abstract FindProductCurrentStateDTO findproductUseCaseStep3(FindProductCurrentStateDTO currentState);
	public abstract FindProductCurrentStateDTO findproductUseCaseStep4(FindProductCurrentStateDTO currentState);
	public abstract FindProductCurrentStateDTO findproductUseCaseStep5(FindProductCurrentStateDTO currentState);
	public abstract FindProductCurrentStateDTO findproductUseCaseStep6(FindProductCurrentStateDTO currentState);


	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep1(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep2(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep3(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep4(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep5(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep6(UpdateProductCurrentStateDTO currentState);
	public abstract UpdateProductCurrentStateDTO updateproductUseCaseStep7(UpdateProductCurrentStateDTO currentState);


	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep1(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep2(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep3(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep4(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep5(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep6(ReplaceProductCurrentStateDTO currentState);
	public abstract ReplaceProductCurrentStateDTO replaceproductUseCaseStep7(ReplaceProductCurrentStateDTO currentState);


	public abstract CreateProductCurrentStateDTO createproductUseCaseStep1(CreateProductCurrentStateDTO currentState);
	public abstract CreateProductCurrentStateDTO createproductUseCaseStep2(CreateProductCurrentStateDTO currentState);
	public abstract CreateProductCurrentStateDTO createproductUseCaseStep3(CreateProductCurrentStateDTO currentState);
	public abstract CreateProductCurrentStateDTO createproductUseCaseStep4(CreateProductCurrentStateDTO currentState);
	public abstract CreateProductCurrentStateDTO createproductUseCaseStep5(CreateProductCurrentStateDTO currentState);
	public abstract CreateProductCurrentStateDTO createproductUseCaseStep6(CreateProductCurrentStateDTO currentState);


	public abstract DeleteProductCurrentStateDTO deleteproductUseCaseStep1(DeleteProductCurrentStateDTO currentState);
	public abstract DeleteProductCurrentStateDTO deleteproductUseCaseStep2(DeleteProductCurrentStateDTO currentState);
	public abstract DeleteProductCurrentStateDTO deleteproductUseCaseStep3(DeleteProductCurrentStateDTO currentState);
	public abstract DeleteProductCurrentStateDTO deleteproductUseCaseStep4(DeleteProductCurrentStateDTO currentState);
	public abstract DeleteProductCurrentStateDTO deleteproductUseCaseStep5(DeleteProductCurrentStateDTO currentState);


// Supporting Exception classes

// Supporting DTO classes


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Get a specific product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class GetASpecificProductCurrentStateDTO {
		
		private GetASpecificProductInputParametersDTO inputs;
		private GetASpecificProductReturnDTO returnObject;
		private GetASpecificProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public GetASpecificProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public GetASpecificProductCurrentStateDTO(GetASpecificProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public GetASpecificProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(GetASpecificProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public GetASpecificProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public GetASpecificProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new GetASpecificProductInputParametersDTO();
			returnObject = new GetASpecificProductReturnDTO();
			errorState = new GetASpecificProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Get a specific product
	 */
	public static class GetASpecificProductReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Get a specific product when an exception has been thrown.
	 */
	public static class GetASpecificProductReturnStatusDTO {

		private String exceptionMessage;

		public GetASpecificProductReturnStatusDTO() {
		}

		public GetASpecificProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Get a specific product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class GetASpecificProductInputParametersDTO {


		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<GetASpecificProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<GetASpecificProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<GetASpecificProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Find product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class FindProductCurrentStateDTO {
		
		private FindProductInputParametersDTO inputs;
		private FindProductReturnDTO returnObject;
		private FindProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public FindProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public FindProductCurrentStateDTO(FindProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public FindProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(FindProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public FindProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public FindProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new FindProductInputParametersDTO();
			returnObject = new FindProductReturnDTO();
			errorState = new FindProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Find product
	 */
	public static class FindProductReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Find product when an exception has been thrown.
	 */
	public static class FindProductReturnStatusDTO {

		private String exceptionMessage;

		public FindProductReturnStatusDTO() {
		}

		public FindProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Find product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class FindProductInputParametersDTO {


		private Integer offset;

		private Integer pagesize;

		public Integer getOffset() {
			return offset;
		}

		public void setOffset(Integer offset) {
			this.offset = offset;
		}

		public Integer getPagesize() {
			return pagesize;
		}

		public void setPagesize(Integer pagesize) {
			this.pagesize = pagesize;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<FindProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Update product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class UpdateProductCurrentStateDTO {
		
		private UpdateProductInputParametersDTO inputs;
		private UpdateProductReturnDTO returnObject;
		private UpdateProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public UpdateProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public UpdateProductCurrentStateDTO(UpdateProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public UpdateProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(UpdateProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public UpdateProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public UpdateProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new UpdateProductInputParametersDTO();
			returnObject = new UpdateProductReturnDTO();
			errorState = new UpdateProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Update product
	 */
	public static class UpdateProductReturnDTO {

	};

	/**
	 * Holds the return value for the operation Update product when an exception has been thrown.
	 */
	public static class UpdateProductReturnStatusDTO {

		private String exceptionMessage;

		public UpdateProductReturnStatusDTO() {
		}

		public UpdateProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Update product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class UpdateProductInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<UpdateProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<UpdateProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<UpdateProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Replace product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class ReplaceProductCurrentStateDTO {
		
		private ReplaceProductInputParametersDTO inputs;
		private ReplaceProductReturnDTO returnObject;
		private ReplaceProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public ReplaceProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public ReplaceProductCurrentStateDTO(ReplaceProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public ReplaceProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(ReplaceProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public ReplaceProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public ReplaceProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new ReplaceProductInputParametersDTO();
			returnObject = new ReplaceProductReturnDTO();
			errorState = new ReplaceProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Replace product
	 */
	public static class ReplaceProductReturnDTO {

	};

	/**
	 * Holds the return value for the operation Replace product when an exception has been thrown.
	 */
	public static class ReplaceProductReturnStatusDTO {

		private String exceptionMessage;

		public ReplaceProductReturnStatusDTO() {
		}

		public ReplaceProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Replace product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class ReplaceProductInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<ReplaceProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<ReplaceProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<ReplaceProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Create product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class CreateProductCurrentStateDTO {
		
		private CreateProductInputParametersDTO inputs;
		private CreateProductReturnDTO returnObject;
		private CreateProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public CreateProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public CreateProductCurrentStateDTO(CreateProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public CreateProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(CreateProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public CreateProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public CreateProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new CreateProductInputParametersDTO();
			returnObject = new CreateProductReturnDTO();
			errorState = new CreateProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Create product
	 */
	public static class CreateProductReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Create product when an exception has been thrown.
	 */
	public static class CreateProductReturnStatusDTO {

		private String exceptionMessage;

		public CreateProductReturnStatusDTO() {
		}

		public CreateProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Create product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class CreateProductInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<CreateProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Delete product.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class DeleteProductCurrentStateDTO {
		
		private DeleteProductInputParametersDTO inputs;
		private DeleteProductReturnDTO returnObject;
		private DeleteProductReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public DeleteProductCurrentStateDTO() {
			initialiseDTOs();
		}

		public DeleteProductCurrentStateDTO(DeleteProductInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public DeleteProductInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(DeleteProductReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public DeleteProductReturnStatusDTO getErrorState() {
			return errorState;
		}

		public DeleteProductReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new DeleteProductInputParametersDTO();
			returnObject = new DeleteProductReturnDTO();
			errorState = new DeleteProductReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Delete product
	 */
	public static class DeleteProductReturnDTO {

	};

	/**
	 * Holds the return value for the operation Delete product when an exception has been thrown.
	 */
	public static class DeleteProductReturnStatusDTO {

		private String exceptionMessage;

		public DeleteProductReturnStatusDTO() {
		}

		public DeleteProductReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Delete product in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class DeleteProductInputParametersDTO {


		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<DeleteProductInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<DeleteProductInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<DeleteProductInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};

}