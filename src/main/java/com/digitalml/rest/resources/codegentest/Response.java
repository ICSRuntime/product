package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "product"
  ],
  "type": "object",
  "properties": {
    "product": {
      "$ref": "Product"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.insurance.general.policy.Product product;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    product = new com.digitalml.insurance.general.policy.Product();
	}
	public com.digitalml.insurance.general.policy.Product getProduct() {
		return product;
	}
	
	public void setProduct(com.digitalml.insurance.general.policy.Product product) {
		this.product = product;
	}
}