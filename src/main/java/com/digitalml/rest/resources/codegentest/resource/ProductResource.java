package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.ProductService;
	
import com.digitalml.rest.resources.codegentest.service.ProductService.GetASpecificProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.GetASpecificProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.GetASpecificProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.FindProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.FindProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.FindProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.UpdateProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.UpdateProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.UpdateProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.ReplaceProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.ReplaceProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.ReplaceProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.CreateProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.CreateProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.CreateProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.DeleteProductReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.DeleteProductReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.DeleteProductInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Product
	 * Access and update of insurance product information.0
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class ProductResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private ProductService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private ProductService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof ProductService)) {
			LOGGER.error(implementationClass + " is not an instance of " + ProductService.class.getName());
			return null;
		}

		return (ProductService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificproduct
		Gets product details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/product/{id}")
	public javax.ws.rs.core.Response getaspecificproduct(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificProductInputParametersDTO inputs = new ProductService.GetASpecificProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificProductReturnDTO returnValue = delegateService.getaspecificproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findproduct
		Gets a collection of product details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/product")
	public javax.ws.rs.core.Response findproduct(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindProductInputParametersDTO inputs = new ProductService.FindProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindProductReturnDTO returnValue = delegateService.findproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateproduct
		Updates product

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/product/{id}")
	public javax.ws.rs.core.Response updateproduct(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateProductInputParametersDTO inputs = new ProductService.UpdateProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateProductReturnDTO returnValue = delegateService.updateproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceproduct
		Replaces product

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/product/{id}")
	public javax.ws.rs.core.Response replaceproduct(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceProductInputParametersDTO inputs = new ProductService.ReplaceProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceProductReturnDTO returnValue = delegateService.replaceproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createproduct
		Creates product

	Non-functional requirements:
	*/
	
	@POST
	@Path("/product")
	public javax.ws.rs.core.Response createproduct(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateProductInputParametersDTO inputs = new ProductService.CreateProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateProductReturnDTO returnValue = delegateService.createproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteproduct
		Deletes product

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/product/{id}")
	public javax.ws.rs.core.Response deleteproduct(
		@PathParam("id")@NotEmpty String id) {

		DeleteProductInputParametersDTO inputs = new ProductService.DeleteProductInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteProductReturnDTO returnValue = delegateService.deleteproduct(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}