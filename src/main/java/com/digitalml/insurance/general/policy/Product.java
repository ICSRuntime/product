package com.digitalml.insurance.general.policy;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Product:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "carrierCode": {
      "type": "string"
    },
    "cusipNumber": {
      "type": "string"
    },
    "effectiveDate": {
      "type": "string"
    },
    "expirationDate": {
      "type": "string"
    },
    "planName": {
      "type": "string"
    },
    "productCode": {
      "type": "string"
    },
    "dateBasedOn": {
      "type": "string"
    },
    "commScheduleCode": {
      "type": "string"
    },
    "interestRateClass": {
      "type": "string"
    },
    "defaultCommCode": {
      "type": "string"
    }
  }
}
*/

public class Product {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String carrierCode;

	@Size(max=1)
	private String cusipNumber;

	@Size(max=1)
	private String effectiveDate;

	@Size(max=1)
	private String expirationDate;

	@Size(max=1)
	private String planName;

	@Size(max=1)
	private String productCode;

	@Size(max=1)
	private String dateBasedOn;

	@Size(max=1)
	private String commScheduleCode;

	@Size(max=1)
	private String interestRateClass;

	@Size(max=1)
	private String defaultCommCode;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    carrierCode = null;
	    cusipNumber = null;
	    effectiveDate = null;
	    expirationDate = null;
	    planName = null;
	    productCode = null;
	    dateBasedOn = null;
	    commScheduleCode = null;
	    interestRateClass = null;
	    defaultCommCode = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getCusipNumber() {
		return cusipNumber;
	}
	
	public void setCusipNumber(String cusipNumber) {
		this.cusipNumber = cusipNumber;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getPlanName() {
		return planName;
	}
	
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getProductCode() {
		return productCode;
	}
	
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getDateBasedOn() {
		return dateBasedOn;
	}
	
	public void setDateBasedOn(String dateBasedOn) {
		this.dateBasedOn = dateBasedOn;
	}
	public String getCommScheduleCode() {
		return commScheduleCode;
	}
	
	public void setCommScheduleCode(String commScheduleCode) {
		this.commScheduleCode = commScheduleCode;
	}
	public String getInterestRateClass() {
		return interestRateClass;
	}
	
	public void setInterestRateClass(String interestRateClass) {
		this.interestRateClass = interestRateClass;
	}
	public String getDefaultCommCode() {
		return defaultCommCode;
	}
	
	public void setDefaultCommCode(String defaultCommCode) {
		this.defaultCommCode = defaultCommCode;
	}
}