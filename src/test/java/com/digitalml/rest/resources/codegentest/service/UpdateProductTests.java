package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ProductService.UpdateProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.UpdateProductReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateProductTests {

	@Test
	public void testOperationUpdateProductBasicMapping()  {
		ProductServiceDefaultImpl serviceDefaultImpl = new ProductServiceDefaultImpl();
		UpdateProductInputParametersDTO inputs = new UpdateProductInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		UpdateProductReturnDTO returnValue = serviceDefaultImpl.updateproduct(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}