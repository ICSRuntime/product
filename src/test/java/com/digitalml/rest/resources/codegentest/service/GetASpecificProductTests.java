package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ProductService.GetASpecificProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.GetASpecificProductReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificProductTests {

	@Test
	public void testOperationGetASpecificProductBasicMapping()  {
		ProductServiceDefaultImpl serviceDefaultImpl = new ProductServiceDefaultImpl();
		GetASpecificProductInputParametersDTO inputs = new GetASpecificProductInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificProductReturnDTO returnValue = serviceDefaultImpl.getaspecificproduct(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}