package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ProductService.DeleteProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.DeleteProductReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteProductTests {

	@Test
	public void testOperationDeleteProductBasicMapping()  {
		ProductServiceDefaultImpl serviceDefaultImpl = new ProductServiceDefaultImpl();
		DeleteProductInputParametersDTO inputs = new DeleteProductInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteProductReturnDTO returnValue = serviceDefaultImpl.deleteproduct(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}