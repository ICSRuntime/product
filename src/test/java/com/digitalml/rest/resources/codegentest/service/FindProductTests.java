package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ProductService.FindProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.FindProductReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindProductTests {

	@Test
	public void testOperationFindProductBasicMapping()  {
		ProductServiceDefaultImpl serviceDefaultImpl = new ProductServiceDefaultImpl();
		FindProductInputParametersDTO inputs = new FindProductInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindProductReturnDTO returnValue = serviceDefaultImpl.findproduct(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}