package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Product.ProductServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ProductService.CreateProductInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ProductService.CreateProductReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateProductTests {

	@Test
	public void testOperationCreateProductBasicMapping()  {
		ProductServiceDefaultImpl serviceDefaultImpl = new ProductServiceDefaultImpl();
		CreateProductInputParametersDTO inputs = new CreateProductInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreateProductReturnDTO returnValue = serviceDefaultImpl.createproduct(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}