package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class ProductTests {

	@Test
	public void testResourceInitialisation() {
		ProductResource resource = new ProductResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificproduct(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.findproduct(0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.updateproduct(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationReplaceProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceproduct(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.createproduct(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteProductNoSecurity() {
		ProductResource resource = new ProductResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteproduct(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}